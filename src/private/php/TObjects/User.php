<?php
require_once("Bundle.php");
Class User
{
    public  $id;
    public  $name;
    public  $pass;
    public  $access;

    private $pdo;

    /* Initializes the Database connection of the instance. */    
    
    public function __construct()
    {
        $this->pdo = connect(); 
    }

    /* Logs the user in with a username-password combination. */ 

    public function login()
    {
        $hashword = sha1($this->password);
        $queryOne = $this->pdo->prepare("SELECT COUNT(*) FROM user WHERE name = :name AND password = :password");
        $queryOne->execute(["name"=>$this->name,"password"=>$hashword]);
        $rowNumber = $queryOne->fetchColumn();
        if($rowNumber === "1") {
            return true;
        } else {
            return false; 
        }
    }
    
    /* Grabs the rest of a user's details based on a field. */

    public function populate(array $param)
    {
        $queryOne = $this->pdo->prepare("SELECT * FROM user WHERE".$param[0]."  = :".$param[0]);
        $queryOne->execute([$param[0]=>$param[1]]);

        $result = $queryOne->fetch(PDO::FETCH_OBJ);
        $this->id = $result->id;
        $this->name = $result->name;
        $this->access = $result->access;
    }

    /* This function writes a user to a database, tenatively for use by an admin account. */

    public static function write($name, $password, $access)
    {
        $queryOne = $this->pdo->prepare("INSERT INTO user (name,password, access) VALUES(:name,:password, :access)");
        $queryOne->execute(["name"=>$name,"password"=>$hashword, "access"=>$access]);
    }

    /* Removes a user from the database. */

    public function delete($name)
    {
        deleteTObject($this, "user"); 
    }   

    /* Checks if the user is logged in. */

    public static function isLoggedIn()
    {
        if($_SESSION["loggedin"] === 0 || !isset($_SESSION["loggedin"]))
        {
            return false; 
        } else {
            return true;
        }
    }
    
    /* Self explanatory. */

    public static function logout()
    {
        session_destroy();
        header("Location: login.php?logggedout=1");
    }
}
?>




