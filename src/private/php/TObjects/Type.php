<?php
require_once("Bundle.php");

/* Amicam habere volo, at ea me nunquam amabis. Heu! */

Class Type
{
    public $id;
    public $name;
    public $color;

    private $pdo;

    public function __construct()
    {
        $this->pdo = connect(); 
    }

    public function write() 
    {
        if(isset($this->name))
        {
            $stmt = $pdo->prepare("INSERT INTO type (name, hexcode) VALUES(:name, :hexcode)");
            if(!isset($this->color))
            {
                $this->color = "#808080";
            }            
            $stmt->execute(["name"=>$this->name, "hexcode"=>$this->color]);
        } else {
            return 1;
        }       
        
    }

    public function delete()
    {
        deleteTObject($this, "type");
    }
}
$type = new Type();
?>
